from flask import Flask
from configuration.config import HOST_ADDRESS, PORT, KEY_APP
from views.views_app import index, users
from db.db import db_session


app = Flask(__name__)

app.add_url_rule("/index", view_func=index, methods=["GET", "POST"])
app.add_url_rule("/", view_func=index, methods=["GET", "POST"])
app.add_url_rule("/api/users", view_func=users, methods=["GET", "POST", 'PUT', 'DELETE'])


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    app.secret_key = KEY_APP
    app.debug = True
    app.run(HOST_ADDRESS, port=int(PORT), threaded=True)
