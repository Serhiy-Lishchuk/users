Users

if use the rest api of the service you will need to pass the basic authorization.
Example: {"username": "admin@gmail.com", "password": "ewefea"}


GET request: getting users list or user profile by id.
Example: headers = {"user_id": 1}


POST request: creating user (only admin).
        Example: "json" {
            "role": "user",
            "email": "del@gmail.com",
            "password": "wef",
            "name": "Jhon",
            "last_name": "Travolta",
            "phone": "09811313"
        }


PUT request: update user profile.
        Example: "json" {
            "id": 3,
            "role": "user",
            "email": "del@gmail.com",
            "password": "wef",
            "name": "Jhon",
            "last_name": "Travolta",
            "phone": "09811313"
        }


DELETE request: delete user account (only admin)
Example: headers = {"user_id": 3}


It is recommended to start the project using the docker-compose file of the following content:
    version: '2'


      services:
        users:
          restart: always
          image: registry.gitlab.com/serhiy-lishchuk/users
          command: python3 -u app.py
          ports:
            - 2025:2025

