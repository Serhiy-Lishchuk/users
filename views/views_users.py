import json
from service.get_users import get_user_by_id, get_users_all
from service.create_user import create_user


def users_get(request, user_check):
    """
    Getting information about users
    :param request: request object
    :param user_check: user status
    :return: info about users or answer status.
    """
    if user_check is not None:
        # get user id from headers
        user_id = request.headers.get('user_id')
        if user_id is None:
            # return list of users
            answer = get_users_all(user_check.role)
            return json.dumps(answer)
        else:
            # return user profile by id
            simple_answer = get_user_by_id(user_id, user_check.role)
            return json.dumps(simple_answer)
    return json.dumps({"answer": "ok"})


def users_post(request, user_check):
    """
    Creating new user
    :param request: request object
    :param user_check: user status
    :return: user profile or status for after creating new user.
    """
    if user_check is not None and user_check.role == 'admin':
        # create new user
        new_user = create_user(request.json)
        if new_user is not False:
            answer = {'user_id': new_user.user_id,
                      'name': new_user.user_name,
                      'last_name': new_user.user_last_name,
                      'phone': new_user.user_phone,
                      'role': new_user.user.role,
                      'email': new_user.user.email}
            return json.dumps(answer)
        return json.dumps({'error': 'some wrong'})
    return json.dumps({'error': 'authorization'})
