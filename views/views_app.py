import json
from flask import request
from service.auth import check_auth
from service.update_profile_user import update_user
from service.delete_user import delete_user
from views.views_users import users_get, users_post


def index():
    return json.dumps({"answer": "ok"})


def users():
    """
    REST API: creating, updating, and deleting users.
    :return: answer for request
    """
    # basic authentication
    user_check = check_auth(request.authorization)
    # get list users or user by id
    if request.method == 'GET':
        status_get = users_get(request, user_check)
        return status_get
    # create new user
    elif request.method == 'POST':
        status_post = users_post(request, user_check)
        return status_post
    # update user profile
    elif request.method == 'PUT':
        update_answer = update_user(request.json, user_check)
        return json.dumps(update_answer)
    # delete user by id
    elif request.method == 'DELETE':
        delete_answer = delete_user(request, user_check)
        return json.dumps(delete_answer)
    return json.dumps({"answer": "ok"})

