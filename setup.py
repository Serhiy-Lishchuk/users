from setuptools import setup, find_packages

setup(
    name='users',
    version='1.01',
    description='Users',
    long_description='Users',
    url='https://repo.ktc-ua.com/serhiy.lishchuk/scanner.git',
    author='Serhiy Lishchuk',
    author_email='mcseruy@gmail.com',
    license='MIT',
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5'
    ],
    keywords='users',
    packages=find_packages(),
    install_requires=[
        'flask==0.12.2',
        'SQLAlchemy==1.2.11'

    ]
)
