FROM ubuntu:18.04

EXPOSE 2025

WORKDIR /users

COPY . /users

ENV TZ 'Europe/Kiev'

RUN apt-get update \
&& echo $TZ > /etc/timezone \
&& apt-get install -y tzdata \
&& rm /etc/localtime \
&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
&& dpkg-reconfigure -f noninteractive tzdata \
&& apt-get clean \
&& apt-get install -y nginx python3-pip \
&& pip3 install -e . \
&& ln -s /users/users_nginx.conf /etc/nginx/sites-enabled/ \
&& mkdir -p /tmp/socket \
&& chown www-data:www-data /tmp/socket \
&& rm -rf /var/lib/apt/lists/*


ENV HOST=0.0.0.0 \
PORT=2025 \
GITLAB_URL=https://gitlab.com/Serhiy-Lishchuk/users.git
