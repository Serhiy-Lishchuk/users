from db.models import UserDetail


def get_users_all(role):
    """
    The process of getting information about users
    :param role: user role
    :return: users profile list
    """
    users_list = []
    users_db = UserDetail.query.all()
    # get all user info
    if role == 'admin':
        for x in users_db:
            users_list.append({'user_id': x.user_id,
                               'name': x.user_name,
                               'last_name': x.user_last_name,
                               'phone': x.user_phone,
                               'role': x.user.role,
                               'email': x.user.email})
    # get only simple user info
    else:
        for u in users_db:
            if u.user.role == 'user':
                users_list.append({'user_id': u.user_id,
                                   'name': u.user_name,
                                   'last_name': u.user_last_name,
                                   'phone': u.user_phone,
                                   'role': u.user.role,
                                   'email': u.user.email})
    return users_list


def get_user_by_id(user_id, role):
    """
    The process of getting info about user by id
    :param user_id: user id
    :param role: user role
    :return: user profile info.
    """
    data = {'user': 'not found'}
    find_user = UserDetail.query.filter(UserDetail.user_id == user_id).first()
    if find_user is None:
        return data
    else:
        # check user role
        if role == 'admin':
            data = {'user_id': find_user.user_id,
                    'name': find_user.user_name,
                    'last_name': find_user.user_last_name,
                    'phone': find_user.user_phone,
                    'role': find_user.user.role,
                    'email': find_user.user.email}
        else:
            if find_user.user.role == 'user':
                data = {'user_id': find_user.user_id,
                        'name': find_user.user_name,
                        'last_name': find_user.user_last_name,
                        'phone': find_user.user_phone,
                        'role': find_user.user.role,
                        'email': find_user.user.email}
        return data
