from db.models import User, UserDetail
from db.db import db_session


def create_user(create_data):
    """
    The process of creating user account
    :param create_data: new user data
    :return: user profile or process status
    """
    try:
        user = User(role=create_data.get('role'),
                    email=create_data.get('email'),
                    password=create_data.get('password'))
        session = db_session()
        session.add(user)
        session.commit()
        user_detail = UserDetail(user_name=create_data.get('name'),
                                 user_last_name=create_data.get('last_name'),
                                 user_phone=create_data.get('phone'))
        user_detail.user = user
        session.add(user_detail)
        session.commit()
        return user_detail
    except Exception as error:
        return False
