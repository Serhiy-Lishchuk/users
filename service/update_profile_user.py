from db.db import db_session
from db.models import UserDetail


def update_user(update_data, user_auth):
    """
    The process of updating user profile
    :param update_data: data for update user profile
    :param user_auth: user status
    :return: info about user or process status.
    """
    data = {'user': 'not found'}
    find_user = UserDetail.query.filter(UserDetail.user_id == update_data.get('user_id')).first()
    # check user role
    if user_auth.role == 'user':
        if update_data.get('user_id') == user_auth.id:
            update_data['role'] = 'user'
        else:
            find_user = None
            data = {'error': 'not rules for update profile this user'}

    if find_user is None:
        return data
    else:
        try:
            find_user.user_name = update_data.get('name', find_user.user_name)
            find_user.user_last_name = update_data.get('last_name', find_user.user_last_name)
            find_user.user_phone = update_data.get('phone', find_user.user_phone)
            find_user.user.role = update_data.get('role', find_user.user.role)
            find_user.user.email = update_data.get('email', find_user.user.email)
            find_user.user.password = update_data.get('password', find_user.user.password)
            session = db_session()
            session.commit()
            data = {'user_id': find_user.user_id,
                    'name': find_user.user_name,
                    'last_name': find_user.user_last_name,
                    'phone': find_user.user_phone,
                    'role': find_user.user.role,
                    'email': find_user.user.email}
            return data
        except Exception as error:
            print(error)
            return data
