from db.db import db_session
from db.models import User, UserDetail


def delete_user(request_obj, user_auth):
    """
    The Process of deleting user account
    :param request_obj: request object
    :param user_auth: user status
    :return: process status.
    """
    user_id = request_obj.headers.get('user_id')
    find_user = User.query.filter(User.id == user_id).first()
    if find_user is None or find_user.role == 'admin':
        return {'error': 'not found user'}
    else:
        if user_auth.role == 'admin':
            user_detail = UserDetail.query.filter(UserDetail.user_id == find_user.id).first()
            session = db_session()
            session.delete(find_user)
            session.delete(user_detail)
            session.commit()
            return {find_user.id: 'status delete'}
        return {'error': 'not rules'}
