from db.models import User


def check_auth(data):
    """
    Basic authentication process
    :param data: authentication data
    :return: user status.
    """
    if data is None:
        return data
    else:
        email = data.get('username')
        password = data.get('password')
        user = User.query.filter(User.email == email, User.password == password).first()
        return user
