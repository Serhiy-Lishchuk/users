from sqlalchemy import Column, Integer, String, ForeignKey, orm
from db.db import Base


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    role = Column(String(10), unique=False)
    email = Column(String(120), unique=True)
    password = Column(String(50), unique=False)

    def __init__(self, role=None, email=None, password=None):
        self.role = role
        self.email = email
        self.password = password


class UserDetail(Base):
    __tablename__ = 'userdetail'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    user_name = Column(String(30), unique=False)
    user_last_name = Column(String(30), unique=False)
    user_phone = Column(String(20), unique=True)
    user = orm.relationship('User')

    def __init__(self, user_name=None, user_last_name=None, user_phone=None):
        self.user_name = user_name
        self.user_last_name = user_last_name
        self.user_phone = user_phone
