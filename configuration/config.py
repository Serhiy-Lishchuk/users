import configparser
import os

# reading configuration file
path = os.path.realpath(os.path.dirname(__file__))
config = configparser.ConfigParser()
config.read(os.path.join(path, 'config.ini'))


KEY_APP = config.get('SECRET', 'KEY_APP')

# block configuration data to hosting
HOST_ADDRESS = config.get('RUN_APP', 'HOST_ADDRESS')
PORT = config.get('RUN_APP', 'PORT')
